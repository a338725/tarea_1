/*
2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo: >use students; >db.grades.count() ¿Cuántos registros arrojo el comando count?
    800

3) Encuentra todas las calificaciones del estudiante con el id numero 4.
        db.grades.find({"student_id":4},{_id:0, student_id:0,type:0});
    [
    { score: 87.89071881934647 },
    { score: 27.29006335059361 },
    { score: 28.656451042441 },
    { score: 5.244452510818443 }
    ]

4) ¿Cuántos registros hay de tipo exam?
    db.grades.count({type:"exam"});
    200

5) ¿Cuántos registros hay de tipo homework?
    students> db.grades.count({type:"homework"});
    400

6) ¿Cuántos registros hay de tipo quiz?
    students> db.grades.count({type:"quiz"});
    200

7) Elimina todas las calificaciones del estudiante con el id numero 3
    db.grades.update({student_id:3},{$unset:{"score":""}},{multi:true});
    db.grades.find({student_id:3});
    [
    {
        _id: ObjectId("50906d7fa3c412bb040eb584"),
        student_id: 3,
        type: 'quiz'
    },
    {
        _id: ObjectId("50906d7fa3c412bb040eb585"),
        student_id: 3,
        type: 'homework'
    },
    {
        _id: ObjectId("50906d7fa3c412bb040eb586"),
        student_id: 3,
        type: 'homework'
    },
    {
        _id: ObjectId("50906d7fa3c412bb040eb583"),
        student_id: 3,
        type: 'exam'
    }
    ]

8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea?
    db.grades.find({$and:[{type:"homework"},{score:75.29561445722392}]});
    [
    {
        _id: ObjectId("50906d7fa3c412bb040eb59e"),
        student_id: 9,
        type: 'homework',
        score: 75.29561445722392
    }
    ]

9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
    db.grades.update({_id: ObjectId("50906d7fa3c412bb040eb591")}, {$set: {score: 100}})
    {
    acknowledged: true,
    insertedId: null,
    matchedCount: 1,
    modifiedCount: 1,
    upsertedCount: 0
    }


10) A qué estudiante pertenece esta calificación.
     db.grades.find({_id:ObjectId("50906d7fa3c412bb040eb591")});
    [
    {
        _id: ObjectId("50906d7fa3c412bb040eb591"),
        student_id: 6,
        type: 'homework',
        score: 100
    }
    ]
*/
